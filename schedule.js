
var requestJSON = require('request-json');
var bodyParser = require('body-parser');
var express = require('express');

var app = express();
var URLbase = '/apiperu/v1/';
const apiKeyMlab = "apiKey=zispWOu27EwI9Ke7JXSmFO-MkvOK86sx";
const port = process.env.PORT || 3000;
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/apiperudb/collections/";

app.use(bodyParser.json());

app.get(URLbase + 'schedule', function(req, res){
    var httpClient = requestJSON.createClient(urlMlabRaiz);
    var queryString = 'q={"_fields.0.properties.SOURCENAME":"t_pdco_over_indebtedness"}&';

    console.log(URLbase + 'schedule?' + queryString);

    httpClient.get('schedule?' + queryString + apiKeyMlab,
    function(err, respuestaMLab, body){
      var response = {};
      if(err){
        response = {
          "msg" : "Error Obteniendo usuario."
        }
        res.status(500);
      }else{
        if(body.length > 0){
          response = body;
        }else{
          response = {
            "msg" : "Ningun elemento 'user'."
          }
          res.status(404);
        }
      }
    res.send(response);
  });
});

app.post(URLbase + 'login', function(req, res) {
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"_fields.0.properties.SOURCENAME":"' + email +
               '","_fields.0.properties.JOBNAME":"' + password + '"}'

  clienteMlab = requestJSON.createClient(urlMlabRaiz + "schedule?" + query + "&l=1&" + apiKeyMlab)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Login ok
      { clienteMlab = requestJSON.createClient(urlMlabRaiz + "/schedule")
      var cambio = '{"$set":{"_fields.0.properties.logged":true}}'

      clienteMlab.put('?q={"_fields.0.properties.JOBNAME": ' + '"PDCOCD4043"' + '}&' + apiKeyMlab, JSON.parse(cambio),
      //clienteMlab.put('user/' + body[0]._id.$oid + '}&' + apiKey, JSON.parse(cambio),
      function(errP, resP, bodyP) {
        res.send({"login":"ok", "id":body[0].FOLDER, "nombre":body[0].FOLDER, "apellidos":body[0].FREQUENCY})
      }) } else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

app.post(URLbase + 'logout', function(req, res) {
  var id = req.headers.id
  var query = 'q={"_fields.0.properties.JOBNAME":"' + id + '","_fields.0.properties.logged":true}'
  clienteMlab = requestJSON.createClient(urlMlabRaiz + "schedule?" + query + "&l=1&" + apiKeyMlab)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Estaba logado
      { clienteMlab = requestJSON.createClient(urlMlabRaiz + "/schedule")
      var cambio = '{"$set":{"_fields.0.properties.logged":false}}'
      clienteMlab.put('?q={"_fields.0.properties.JOBNAME": ' + '"PDCOCD4043"' + '}&' + apiKeyMlab, JSON.parse(cambio),
      //clienteMlab.put('user/' + respuesta._id.$oid + ? + apiKey, JSON.parse(cambio),
      function(errP, resP, bodyP) {
        res.send({"logout":"ok", "id":body[0].id})
      })
    }
    else {
      res.status(200).send('Usuario no logado previamente')
}
}
    })
  })


app.listen(port);
