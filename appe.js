var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var URLbase = '/apiperu/v1/';

app.use(bodyParser.json());

//GET users
app.get(URLbase + 'users',
  function(req, res){
    console.log('GET /apiperu/v1/users');
    //res.status(200).send({"msg":"respuesta correcta"});
    res.send(userFile);
  });

//GET users por ID
app.get (URLbase + 'users/:id',
  function(req, res){
    console.log('GET /apiperu/v1/users/:id');
    console.log(req.params);
    console.log(req.params.id);
    let pos = req.params.id;
    res.send(userFile[pos-1]);
  });

//POST users
app.post(URLbase + 'users',
  function(req, res){
    console.log('POST /apiperu/v1/users');
    let newID = userFile.length + 1;
    let newUser = {
      "userId" : newID,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "gender" : req.body.gender,
      "ip_address" : req.body.ip_adddress
    };
    userFile.push(newUser);
    console.log(userFile);
    res.status(201);
    res.send({"msg":"usuario añadido correctamente", newUser});
  });






//app.get('/', function (req, res) {
//  res.send('Good Mornings! and have a good day!!');
//});

app.listen(port);
