
var requestJSON = require('request-json');
var bodyParser = require('body-parser');
var express = require('express');

var app = express();
var port = process.env.PORT || 3000;
var URLbase = '/apiperu/v1/';
const apiKeyMlab = "apiKey=zispWOu27EwI9Ke7JXSmFO-MkvOK86sx";
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/apiperudb/collections/";

//var clienteMlab

app.use(bodyParser.json());

app.get(URLbase + 'users',function(req, res){
  console.log('GET /apiperu/v1/users');

  var httpClient = requestJSON.createClient(urlMlabRaiz);
  var queryString = 'f={"_id":0}&';
  httpClient.get('user?' + queryString + apiKeyMlab,
  function(err, respuestaMLab, body){
    var response = {};
    if(err){
      response = {
        "msg" : "Error Obteniendo usuario."
      }
      res.status(500);
    }else{
      if(body.length > 0){
        response = body;
      }else{
        response = {
          "msg" : "Ningun elemento 'user'."
        }
        res.status(404);
      }
    }
    res.send(response);
  });
});

//PUT of user
app.put(URLbase + 'users/:id',function(req, res) {
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = requestJSON.createClient(baseMLabURL);
  clienteMlab.get('user?'+ queryStringID + apikeyMLab ,
  function(error, respuestaMLab , body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    clienteMlab.put(baseMLabURL + 'user?q={"id": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
    function(error, respuestaMLab, body) {
      console.log("body:"+ body);
      res.send(body);
    });
  });
});

//DELETE user with id
app.delete(URLbase + "users/:id",function(req, res){
  console.log("entra al DELETE");
  console.log("request.params.id: " + req.params.id);
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('user?' +  queryStringID + apikeyMLab,
  function(error, respuestaMLab, body){
    var respuesta = body[0];
    console.log("body delete:"+ respuesta);
    httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
    function(error, respuestaMLab,body){
      res.send(body);
    });
  });
});

app.post('/apitechu/v5/login', function(req, res) {
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Login ok
      { clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
      var cambio = '{"$set":{"logged":true}}'
      clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio),
      //clienteMlab.put('user/' + body[0]._id.$oid + '}&' + apiKey, JSON.parse(cambio),
      function(errP, resP, bodyP) {
        res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellidos":body[0].apellidos})
      }) } else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

app.post('/apitechu/v5/logout', function(req, res) {
  var id = req.headers.id
  var query = 'q={"id":' + id + ', "logged":true}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Estaba logado
      { clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
      var cambio = '{"$set":{"logged":false}}'
      clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio),
      //clienteMlab.put('user/' + respuesta._id.$oid + ? + apiKey, JSON.parse(cambio), 
      function(errP, resP, bodyP) {
        res.send({"logout":"ok", "id":body[0].id})
      })
    }
    else {
      res.status(200).send('Usuario no logado previamente')
    }
  }

  app.listen(port);
